#!/usr/bin/env bash

# Make sure to activate docker service and the cloudera container before running this script
ready=$(docker inspect -f '{{.State.Running}}' cloudera)

if [ "$ready" != "true" ]; then
    if [ "$ready" == "false" ]; then
        echo "Start the cloudera container first"
    else
        echo $ready
    fi
    exit 1
fi

# You must be in a path of the git
source=$(git rev-parse --show-toplevel 2> /dev/null)
if [ $? -eq 128 ]; then
    echo "Your actual path is not in the git. Change it and retry"
    exit 2
fi

docker cp -a -L ${source}/Res/json/. cloudera:/home/cloudera/resources/
docker exec -u cloudera cloudera bash -c "
	mkdir /home/cloudera/output; \
	hadoop fs -mkdir /user/cloudera/input; \
	hadoop fs -put /home/cloudera/resources/* /user/cloudera/input \
"
echo "Resources loaded successfully!"

exit 0
