/*
* Answer to Q3_AverageNumberReview with different time granularities, e.g., in the last month, year, two years.
*/

review = LOAD '$review' USING JsonLoader('review_id: chararray, user_id:chararray, business_id:chararray, stars:float, useful:int, funny:int, cool:int, text:chararray, date:chararray');

-- extract data and filter by date (data is converted multiple times so that the time in it can be ignored)
filterrev = FOREACH review GENERATE FLATTEN(user_id),FLATTEN(business_id),FLATTEN(date);
filterrev = FILTER filterrev BY DaysBetween(ToDate('$startdate','yyyy-MM-dd'),ToDate(ToString(ToDate(date, 'yyyy-MM-dd HH:mm:ss'), 'yyyy-MM-dd'),'yyyy-MM-dd')) * DaysBetween(ToDate(ToString(ToDate(date, 'yyyy-MM-dd HH:mm:ss'), 'yyyy-MM-dd'),'yyyy-MM-dd'),ToDate('$enddate','yyyy-MM-dd'))>=(long)0;
users = GROUP filterrev BY user_id;
reviews_per_user = FOREACH users GENERATE FLATTEN(group) AS user_id, COUNT($1) AS counter;

res = JOIN filterrev by user_id, reviews_per_user by user_id;

-- group by key to calculate the average
res = GROUP res BY filterrev::business_id;
result = FOREACH res GENERATE FLATTEN(group) AS business_id, AVG($1.reviews_per_user::counter) AS average;

STORE result INTO '$out';
