/*
* For each user, provide the average difference between the vote she/he gave to a business, and
* the average vote that business has. For instance, if the user visited
* two business whose average score is 4.5 and 4.8,
* and she/he gave 3 to both of them, then the average difference would be: ((4.5-3) + (4.8-3))/2;
*/

review = LOAD '$review' USING JsonLoader('review_id: chararray, user_id:chararray, business_id:chararray, stars:float');
business = LOAD '$business' USING JsonLoader('business_id:chararray,name:chararray,address:chararray,city:chararray,state:chararray,postal_code:chararray,latitude:chararray,longitude:chararray,stars:float');

-- extract and join data
review_red = FOREACH review GENERATE FLATTEN(user_id),FLATTEN(business_id),FLATTEN(stars);
business_red = FOREACH business GENERATE FLATTEN(business_id),FLATTEN(stars);
res = JOIN review_red BY business_id, business_red BY business_id;

-- group by key to calculate the average
res = GROUP res BY review_red::user_id;

result = FOREACH res {
    concat = FOREACH res GENERATE (business_red::stars - review_red::stars); --it will iterate only over the records of the inpt bag
    GENERATE group, AVG(concat);
};

STORE result INTO '$out';
