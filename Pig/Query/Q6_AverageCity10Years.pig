/*
* Answer to Q5_AverageCity showing the evolution over time, e.g., the average rating per year in the
* last 10 years.
*/

review = LOAD '$review' USING JsonLoader('review_id: chararray, user_id:chararray, business_id:chararray, stars:float, useful:int, funny:int, cool:int, text:chararray, date:chararray');
business = LOAD '$business' USING JsonLoader('business_id:chararray,name:chararray,address:chararray,city:chararray,state:chararray,postal_code:chararray,latitude:chararray,longitude:chararray,stars:float');

-- extract data and filter by last 10 years
review_red = FOREACH review GENERATE FLATTEN(business_id),FLATTEN(stars),GetYear(CurrentTime())-GetYear(ToDate(date,'yyyy-MM-dd HH:mm:ss')) AS year;
review_red = FILTER review_red BY year<10;

--intermediate result
business_red = FOREACH business GENERATE FLATTEN(business_id),FLATTEN(city);

res = JOIN business_red by business_id, review_red by business_id  ;
res = GROUP res BY business_red::city;
-- create a filter for each year and apply it to the result
result = FOREACH res {
    filter0 = FILTER $1 BY review_red::year == 0;
    filter1 = FILTER $1 BY review_red::year == 1;
    filter2 = FILTER $1 BY review_red::year == 2;
    filter3 = FILTER $1 BY review_red::year == 3;
    filter4 = FILTER $1 BY review_red::year == 4;
    filter5 = FILTER $1 BY review_red::year == 5;
    filter6 = FILTER $1 BY review_red::year == 6;
    filter7 = FILTER $1 BY review_red::year == 7;
    filter8 = FILTER $1 BY review_red::year == 8;
    filter9 = FILTER $1 BY review_red::year == 9;
    GENERATE FLATTEN(group) AS city,
    (CASE WHEN IsEmpty(filter0) THEN (float)0.0 ELSE AVG(filter0.review_red::stars) END) AS avg0,
    (CASE WHEN IsEmpty(filter1) THEN (float)0.0 ELSE AVG(filter1.review_red::stars) END) AS avg1,
    (CASE WHEN IsEmpty(filter2) THEN (float)0.0 ELSE AVG(filter2.review_red::stars) END) AS avg2,
    (CASE WHEN IsEmpty(filter3) THEN (float)0.0 ELSE AVG(filter3.review_red::stars) END) AS avg3,
    (CASE WHEN IsEmpty(filter4) THEN (float)0.0 ELSE AVG(filter4.review_red::stars) END) AS avg4,
    (CASE WHEN IsEmpty(filter5) THEN (float)0.0 ELSE AVG(filter5.review_red::stars) END) AS avg5,
    (CASE WHEN IsEmpty(filter6) THEN (float)0.0 ELSE AVG(filter6.review_red::stars) END) AS avg6,
    (CASE WHEN IsEmpty(filter7) THEN (float)0.0 ELSE AVG(filter7.review_red::stars) END) AS avg7,
    (CASE WHEN IsEmpty(filter8) THEN (float)0.0 ELSE AVG(filter8.review_red::stars) END) AS avg8,
    (CASE WHEN IsEmpty(filter9) THEN (float)0.0 ELSE AVG(filter9.review_red::stars) END) AS avg9;

};

result = ORDER result BY city ASC;
--result = FILTER result BY city!='';

STORE result INTO '$out';
