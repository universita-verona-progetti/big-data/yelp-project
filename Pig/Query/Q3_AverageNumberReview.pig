/*
* For each business, provide the average number of reviews of the users that wrote a review
* for that business. For instance, if user A and B wrote a review for a business,
* and user A has written 50 reviews in total, while user B has written 30 reviews in total,
* then the average number of reviews of
* the users that wrote a review for that business is (50+30)/2;
*/

review = LOAD '$review' USING JsonLoader('review_id: chararray, user_id:chararray, business_id:chararray, stars:float, useful:int, funny:int, cool:int, text:chararray, date:chararray');

-- extract data
review_drop = FOREACH review GENERATE FLATTEN(user_id),FLATTEN(business_id);
users = GROUP review_drop BY user_id;
reviews_per_user = FOREACH users GENERATE FLATTEN(group) AS user_id, COUNT($1) AS counter;

res = JOIN review_drop by user_id, reviews_per_user by user_id;

-- group by key to calculate the average
res = GROUP res BY review_drop::business_id;
result = FOREACH res GENERATE FLATTEN(group) AS business_id, AVG($1.reviews_per_user::counter) AS average;

STORE result INTO '$out';
