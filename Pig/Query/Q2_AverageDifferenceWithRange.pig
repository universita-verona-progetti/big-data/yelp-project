/*
Answer to Q1_AverageDifference with different time granularities, e.g., in the last month, year, two years.
*/

business = LOAD '$business' USING JsonLoader('business_id:chararray,name:chararray,address:chararray,city:chararray,state:chararray,postal_code:chararray,latitude:chararray,longitude:chararray,stars:float');
review = LOAD '$review' USING JsonLoader('review_id: chararray, user_id:chararray, business_id:chararray, stars:float, useful:int, funny:int, cool:int, text:chararray, date:chararray');

-- extract and filter by date (data is converted multiple times so that the time in it can be ignored) then join data
review_red = FILTER review BY DaysBetween(ToDate('$startdate','yyyy-MM-dd'),ToDate(ToString(ToDate(date, 'yyyy-MM-dd HH:mm:ss'), 'yyyy-MM-dd'),'yyyy-MM-dd')) * DaysBetween(ToDate(ToString(ToDate(date, 'yyyy-MM-dd HH:mm:ss'), 'yyyy-MM-dd'),'yyyy-MM-dd'),ToDate('$enddate','yyyy-MM-dd'))>=(long)0;
review_red = FOREACH review_red GENERATE FLATTEN(user_id),FLATTEN(business_id),FLATTEN(stars);
business_red = FOREACH business GENERATE FLATTEN(business_id),FLATTEN(stars);
res = JOIN review_red BY business_id, business_red BY business_id;

-- group by key to calculate the average
res = GROUP res BY review_red::user_id;

result = FOREACH res {
    concat = FOREACH res GENERATE (business_red::stars - review_red::stars); --it will iterate only over the records of the inpt bag
    GENERATE group, AVG(concat);
};

STORE result INTO '$out';
