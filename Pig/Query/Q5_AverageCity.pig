/*
* For each city the average rating given for businesses in that city in the last
* month or year;
*/

review = LOAD '$review' USING JsonLoader('review_id: chararray, user_id:chararray, business_id:chararray, stars:float, useful:int, funny:int, cool:int, text:chararray, date:chararray');
business = LOAD '$business' USING JsonLoader('business_id:chararray,name:chararray,address:chararray,city:chararray,state:chararray,postal_code:chararray,latitude:chararray,longitude:chararray,stars:float');

-- extract data and filter by date (data is converted multiple times so that the time in it can be ignored)
filterrev = FOREACH review GENERATE FLATTEN(business_id),FLATTEN(stars),FLATTEN(date);
filterrev = FILTER filterrev BY DaysBetween(ToDate('$startdate','yyyy-MM-dd'),ToDate(ToString(ToDate(date, 'yyyy-MM-dd HH:mm:ss'), 'yyyy-MM-dd'),'yyyy-MM-dd')) * DaysBetween(ToDate(ToString(ToDate(date, 'yyyy-MM-dd HH:mm:ss'), 'yyyy-MM-dd'),'yyyy-MM-dd'),ToDate('$enddate','yyyy-MM-dd'))>=(long)0;
business_red = FOREACH business GENERATE FLATTEN(business_id),FLATTEN(city);

res = JOIN business_red by business_id, filterrev by business_id;

-- group by key to calculate the average
res = GROUP res BY business_red::city;

result = FOREACH res GENERATE FLATTEN(group) AS city, AVG($1.filterrev::stars) AS average;
result = ORDER result BY city ASC;

STORE result INTO '$out';
