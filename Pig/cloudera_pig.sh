#!/usr/bin/env bash

querynumber=$1
repositoryroot=$(git rev-parse --show-toplevel 2>/dev/null)

# transfer pig and init files to cloudera
echo -e "Transferring pig file...\c"
docker cp -a -L "${repositoryroot}"/Pig/Query/. cloudera:/home/cloudera/pig/
docker cp -a -L "${repositoryroot}"/Pig/Conf/. cloudera:/home/cloudera/pig/conf/
echo "OK"

# connect to cloudera and execute commands
echo -e "Cloudera execution started...\c"
docker exec -u cloudera cloudera bash -c " \
    hadoop fs -rm -r -f /user/cloudera/output*; \
    pig -x mapreduce -m /home/cloudera/pig/conf/Q${querynumber}_cloudera.init /home/cloudera/pig/Q${querynumber}*; \
    hadoop fs -getmerge /user/cloudera/output/ /home/cloudera/output/pig.txt; \
" >"${repositoryroot}"/Confronto_risultati/LOG/Q"${querynumber}"/cloudera_pig_"$(date +'%s')".log 2>&1
echo "OK"

# get output
echo -e "Transferring output results...\c"
docker cp -a -L cloudera:/home/cloudera/output/pig.txt "${repositoryroot}"/Confronto_risultati/Cloudera/Q"${querynumber}"/
echo "OK"

echo "DONE!"
exit 0
