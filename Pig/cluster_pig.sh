#!/usr/bin/env bash

querynumber=$1

repositoryroot=$(git rev-parse --show-toplevel 2>/dev/null)
IFS=$'\r\n' GLOBIGNORE='*' command eval 'credentials=($(cat ${repositoryroot}/credentials))'
ipaddr=${credentials[0]}
user=${credentials[1]}
pass=${credentials[2]}

# send pig file
echo -e "Transferring pig file...\c"
sshpass -p "${pass}" scp "${repositoryroot}"/Pig/Query/Q"${querynumber}"* "${user}"@"${ipaddr}":/home/"${user}"/yelp/pig/
sshpass -p "${pass}" scp "${repositoryroot}"/Pig/Conf/Q"${querynumber}"_cluster.init "${user}"@"${ipaddr}":/home/"${user}"/yelp/pig/conf
echo "OK"

# connect to the cluster and execute commands
echo -e "Cluster execution started...\c"
sshpass -p "${pass}" ssh -o "StrictHostKeyChecking no" "${user}"@"${ipaddr}" " \
    hadoop fs -rm -r -f /user/${user}/yelp/output*; \
    export PATH=\$PATH:/home/ubuntu/pig/pig-0.17.0/bin ; \
    pig -x mapreduce -m /home/${user}/yelp/pig/conf/Q${querynumber}_cluster.init /home/${user}/yelp/pig/Q${querynumber}*; \
    hadoop fs -getmerge /user/${user}/yelp/output/ /home/${user}/yelp/output/pig.txt; \
    cd yelp/output; \
    tar -czvf /home/${user}/yelp/output.tgz pig.txt \
" >"${repositoryroot}"/Confronto_risultati/LOG/Q"${querynumber}"/cluster_pig_"$(date +'%s')".log 2>&1
echo "OK"

# transfer output
echo -e "Transferring output results...\c"
sshpass -p "${pass}" scp "${user}"@"${ipaddr}":/home/"${user}"/yelp/output.tgz "${repositoryroot}"/Confronto_risultati/Cluster/Q"${querynumber}"
cd "${repositoryroot}"/Confronto_risultati/Cluster/Q"${querynumber}" || return
tar -xzvf output.tgz >/dev/null
rm output.tgz
echo "OK"

# delete output from cluster filesystem
echo -e "Delete output from cluster filesystem...\c"
sshpass -p "${pass}" ssh -o "StrictHostKeyChecking no" "${user}"@"${ipaddr}" "rm -rf /home/${user}/yelp/output*"
echo "OK"

echo "DONE!"

exit 0
