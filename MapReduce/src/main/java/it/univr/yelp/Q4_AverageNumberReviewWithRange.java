package it.univr.yelp;

import it.univr.yelp.mappers.BusinessSumrev;
import it.univr.yelp.mappers.UserBusinessReview;
import it.univr.yelp.mappers.UserOneReview;
import it.univr.yelp.mappers.UserSumreview;
import it.univr.yelp.reducers.BusinessAVGreview;
import it.univr.yelp.reducers.BusinessReview;
import it.univr.yelp.reducers.UserNumreview;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Answer to the {@link Q3_AverageNumberReview} with different time granularities, e.g., in the last month, year, two years.
 */

public class Q4_AverageNumberReviewWithRange extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Q4_AverageNumberReviewWithRange(), args);
        System.exit(res);

    }

    public int run(String[] args) throws Exception {

        Configuration conf = getConf();
        conf.set("start", args[2]);
        conf.set("end", args[3]);

        Job job1 = Job.getInstance(getConf(), "Q4_AverageNumberReviewWithRange1");
        job1.setJarByClass(this.getClass());
        FileInputFormat.addInputPath(job1, new Path(args[0] + "/input/review.json"));
        FileOutputFormat.setOutputPath(job1, new Path(args[0] + "/int1"));
        job1.setMapperClass(UserOneReview.class);
        job1.setNumReduceTasks(Integer.parseInt(args[1]));
        job1.setReducerClass(UserNumreview.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(IntWritable.class);
        if (!job1.waitForCompletion(true))
            return 1;

        Job job2 = Job.getInstance(getConf(), "Q4_AverageNumberReviewWithRange2");
        job2.setJarByClass(this.getClass());
        MultipleInputs.addInputPath(job2, new Path(args[0] + "/input/review.json"), TextInputFormat.class, UserBusinessReview.class);
        MultipleInputs.addInputPath(job2, new Path(args[0] + "/int1/part*"), TextInputFormat.class, UserSumreview.class);
        FileOutputFormat.setOutputPath(job2, new Path(args[0] + "/int2"));
        job2.setNumReduceTasks(Integer.parseInt(args[1]));
        job2.setPartitionerClass(it.univr.yelp.KeyPartitioner.class);
        job2.setMapOutputKeyClass(TextPair.class);
        job2.setMapOutputValueClass(TextPair.class);
        job2.setOutputKeyClass(Text.class);
        job2.setOutputValueClass(TextPair.class);
        job2.setGroupingComparatorClass(TextPair.FirstComparator.class);
        job2.setReducerClass(BusinessReview.class);
        if (!job2.waitForCompletion(true)) //controlla se il job sta girando o meno
            return 1;

        Job job3 = Job.getInstance(getConf(), "Q4_AverageNumberReviewWithRange3");
        job3.setJarByClass(this.getClass());
        FileInputFormat.addInputPath(job3, new Path(args[0] + "/int2/part*"));
        FileOutputFormat.setOutputPath(job3, new Path(args[0] + "/output"));
        job3.setMapperClass(BusinessSumrev.class);
        job3.setNumReduceTasks(Integer.parseInt(args[1]));
        job3.setReducerClass(BusinessAVGreview.class);
        job3.setOutputKeyClass(Text.class);
        job3.setOutputValueClass(IntWritable.class);
        return job3.waitForCompletion(true) ? 0 : 1; //controlla se il job sta girando o meno

    }

}
