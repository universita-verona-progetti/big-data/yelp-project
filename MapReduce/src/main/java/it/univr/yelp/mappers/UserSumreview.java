package it.univr.yelp.mappers;

import it.univr.yelp.TextPair;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class UserSumreview extends Mapper<LongWritable, Text, TextPair, TextPair> {

    /**
     * The mapper takes an output from the previous reducer UserNumreview as input and it generates
     * a new file given as input for the next reducer in key-value format (TextPair-Textpair).
     * The first TextPair includes user_id and the value 0 (zero is necessary for the key partitioner to sort by key)
     * The second TextPair includes "" (the empty string is useful to apply the ReduceSide Join) and
     * the total of reviews made by that user.
     * <p>
     * This mapper is used by queries 3 and 4.
     *
     * @param offset   byte offset within the file of the beginning of the line
     * @param lineText content of the line
     * @param context  allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    @Override
    public void map(LongWritable offset, Text lineText, Context context) throws IOException, InterruptedException {

        String[] line = lineText.toString().split("\t");

        //id utente
        String userid = line[0];
        //somma
        String sum = line[1];

        context.write(new TextPair(userid, "0"), new TextPair("", sum));
    }
}
