package it.univr.yelp.mappers;

import it.univr.yelp.TextPair;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BusinessYearStarReview extends Mapper<LongWritable, Text, TextPair, TextPair> {

    /**
     * The mapper takes review.json as input, it parses the JSON and it generates a new file
     * given as input for the reducer in key-value format (TextPair-TextPair).
     * The first TextPair includes business_id and 1 (one is necessary for the key partitioner to sort by key)
     * The second TextPair includes the year as a number from 0 to 9 (that's because we make the difference between
     * the current year and the review's one and we want to keep last 10 years' reviews)
     * and the rating in stars given from users in a specific review for that business.
     * <p>
     * This mapper is used by query 6.
     *
     * @param offset   byte offset within the file of the beginning of the line
     * @param lineText content of the line
     * @param context  allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */
    @Override
    public void map(LongWritable offset, Text lineText, Context context) throws IOException, InterruptedException {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        int actualdate = Integer.parseInt(simpleDateFormat.format(new Date()));

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject) jsonParser.parse(lineText.toString());
        } catch (ParseException ignored) {
        }

        int date = Integer.parseInt(jsonObject.get("date").toString().split(" ")[0].split("-")[0]);
        int diff = actualdate - date;

        if (diff <= 9) {
            //id del locale
            String business = jsonObject.get("business_id").toString();
            //stelle date dall'utente
            String star = jsonObject.get("stars").toString();
            // create textpair
            Text year = new Text(Integer.toString(diff));
            //this.currentPair.set(year, star);
            context.write(new TextPair(business, "1"), new TextPair(year, new Text(star)));
        }
    }

}
