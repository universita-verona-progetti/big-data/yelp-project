package it.univr.yelp.mappers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
//import java.util.function.ToDoubleBiFunction;

public class BusinessSumrev extends Mapper<LongWritable, Text, Text, IntWritable> {

    /**
     * The mapper takes an output from the previous reducer BusinessReview as input and it generates
     * a new file given as input for the next reducer in key-value format (Text-IntWritable).
     * The Text is the business_id.
     * The IntWritable is the rating in stars of the business given in a specific review.
     * The goal of this mapper is to sort by business_id in order to simplify the work to the next reducer
     * <p>
     * This mapper is used by queries 3 and 4.
     *
     * @param offset   byte offset within the file of the beginning of the line
     * @param lineText content of the line
     * @param context  allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    @Override
    public void map(LongWritable offset, Text lineText, Context context) throws IOException, InterruptedException {

        String[] line = lineText.toString().split("\t");

        //id utente
        Text business = new Text(line[0]);
        //somma
        IntWritable somma = new IntWritable(Integer.parseInt(line[1]));

        context.write(business, somma);
    }
}
