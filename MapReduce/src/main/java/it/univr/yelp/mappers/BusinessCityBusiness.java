package it.univr.yelp.mappers;

import it.univr.yelp.TextPair;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class BusinessCityBusiness extends Mapper<LongWritable, Text, TextPair, TextPair> {

    /**
     * The mapper takes business.json as input, it parses the JSON and it generates a new file
     * given as input for the reducer in key-value format (TextPair-TextPair).
     * The first TextPair includes business_id and 0 (zero is necessary for the key partitioner to sort by key)
     * The second TextPair includes the city name and "" (the empty string is useful to apply the ReduceSide Join)
     * <p>
     * This mapper is used by queries 5 and 6.
     *
     * @param offset   byte offset within the file of the beginning of the line
     * @param lineText content of the line
     * @param context  allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */
    @Override
    public void map(LongWritable offset, Text lineText, Context context) throws IOException, InterruptedException {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject) jsonParser.parse(lineText.toString());
        } catch (ParseException ignored) {
        }
        //id del locale
        String business = jsonObject.get("business_id").toString();
        //city
        String city = jsonObject.get("city").toString();
        context.write(new TextPair(business, "0"), new TextPair(city, ""));
    }
}

