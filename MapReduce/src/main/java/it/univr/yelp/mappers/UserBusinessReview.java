package it.univr.yelp.mappers;

import it.univr.yelp.TextPair;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserBusinessReview extends Mapper<LongWritable, Text, TextPair, TextPair> {

    /**
     * The mapper takes review.json as input, it parses the JSON and it generates a new file
     * given as input for the reducer in key-value format (TextPair-TextPair).
     * The first TextPair includes user_id and 1 (one is necessary for the key partitioner to sort by key)
     * The second TextPair includes the business_id and "" (the empty string is useful to apply the ReduceSide Join)
     * Furthermore, this mapper is able to filter by a date range passed as arguments.
     * <p>
     * This mapper is used by queries 3 and 4.
     *
     * @param offset   byte offset within the file of the beginning of the line
     * @param lineText content of the line
     * @param context  allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    @Override
    public void map(LongWritable offset, Text lineText, Context context) throws IOException, InterruptedException {

        Configuration conf = context.getConfiguration();
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date start = null;
        Date end = null;
        Date revdate = null;

        if (conf.get("start") != null && conf.get("end") != null) {
            start = new Date();
            end = new Date();
            revdate = new Date();
            try {
                start = simpleDateFormat.parse(conf.get("start"));
                end = simpleDateFormat.parse(conf.get("end"));
            } catch (java.text.ParseException ignored) {
            }
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject) jsonParser.parse(lineText.toString());
        } catch (ParseException ignored) {
        }

        String user = jsonObject.get("user_id").toString();
        String business = jsonObject.get("business_id").toString();

        if (start != null && end != null) {
            String date = jsonObject.get("date").toString().split(" ")[0];
            try {
                revdate = simpleDateFormat.parse(date);
            } catch (java.text.ParseException ignored) {
            }

            if (start.compareTo(revdate) * revdate.compareTo(end) < 0) {

                return;
            }
        }
        context.write(new TextPair(user, "1"), new TextPair(business, ""));
    }
}
