package it.univr.yelp.mappers;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class CityStarsOrd extends Mapper<LongWritable, Text, Text, FloatWritable> {

    /**
     * The mapper takes an output from the previous reducer CityStars as input and it generates
     * a new file given as input for the next reducer in key-value format (Text-FloatWritable).
     * The Text is the city name.
     * The FloatWritable is the rating in stars of a business in that city.
     * The goal of this mapper is to sort by city name in order to simplify the work to the next reducer
     * <p>
     * This mapper is used by query 5.
     *
     * @param offset   byte offset within the file of the beginning of the line
     * @param lineText content of the line
     * @param context  allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    @Override
    public void map(LongWritable offset, Text lineText, Context context) throws IOException, InterruptedException {
        String[] line = lineText.toString().split("\t");


        //id utente
        Text city = new Text(line[0]);
        //stelle differenza
        FloatWritable stars = new FloatWritable(Float.parseFloat(line[1]));

        context.write(city, stars);
    }
}