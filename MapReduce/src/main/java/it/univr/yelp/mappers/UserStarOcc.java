package it.univr.yelp.mappers;

import it.univr.yelp.TextPair;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class UserStarOcc extends Mapper<LongWritable, Text, Text, TextPair> {

    /**
     * The mapper takes an output from the previous reducer UserStarOcc as input and it generates
     * a new file given as input for the next reducer in key-value format (Text-TextPair).
     * The Text is the user_id.
     * The TextPair includes the difference between the business rating and the user rating for that business in that
     * review and the value 1 (one will be useful for the next reducer to count the number of reviews made by the user)
     * <p>
     * This mapper is used by queries 1 and 2.
     *
     * @param offset   byte offset within the file of the beginning of the line
     * @param lineText content of the line
     * @param context  allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    @Override
    public void map(LongWritable offset, Text lineText, Context context) throws IOException, InterruptedException {
        String[] line = lineText.toString().split("\t");


        //id utente
        Text userid = new Text(line[0]);
        //stelle differenza
        Text stars = new Text(line[1].split(";")[0]);
        //occorrenze
        Text occurrency = new Text(line[1].split(";")[1]);

        context.write(userid, new TextPair(stars, occurrency));
    }
}
