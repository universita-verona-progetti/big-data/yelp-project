package it.univr.yelp;

import it.univr.yelp.mappers.BusinessCityBusiness;
import it.univr.yelp.mappers.BusinessYearStarReview;
import it.univr.yelp.mappers.CityStarsPerYearOrd;
import it.univr.yelp.reducers.CityAVGStarsPerYear;
import it.univr.yelp.reducers.CityStarsPerYear;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Answer to the {@link Q5_AverageCity} showing the evolution over time, e.g., the average rating per year in the
 * last 10 years.
 */

public class Q6_AverageCity10Years extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Q6_AverageCity10Years(), args);
        System.exit(res);

    }

    public int run(String[] args) throws Exception {
        Configuration conf = getConf();

        Job job1 = Job.getInstance(conf, "Q6_AverageCity10Years1");
        job1.setJarByClass(this.getClass());
        MultipleInputs.addInputPath(job1, new Path(args[0] + "/input/business.json"), TextInputFormat.class, BusinessCityBusiness.class);
        MultipleInputs.addInputPath(job1, new Path(args[0] + "/input/review.json"), TextInputFormat.class, BusinessYearStarReview.class);
        FileOutputFormat.setOutputPath(job1, new Path(args[0] + "/int1")); // eccezione se la directory esiste già, il job si ferma
        job1.setNumReduceTasks(Integer.parseInt(args[1]));   // setta il numero di reducer, forse è facoltativa, di default 1
        job1.setReducerClass(CityStarsPerYear.class);
        job1.setPartitionerClass(KeyPartitioner.class);
        job1.setMapOutputKeyClass(TextPair.class);
        job1.setMapOutputValueClass(TextPair.class);
        job1.setOutputKeyClass(TextPair.class);
        job1.setOutputValueClass(Text.class);
        job1.setGroupingComparatorClass(TextPair.FirstComparator.class);
        if (!job1.waitForCompletion(true))
            return 1;


        Job job2 = Job.getInstance(conf, "Q6_AverageCity10Years2");
        job2.setJarByClass(this.getClass());
        FileInputFormat.addInputPath(job2, new Path(args[0] + "/int1/part*"));
        FileOutputFormat.setOutputPath(job2, new Path(args[0] + "/output"));
        job2.setMapperClass(CityStarsPerYearOrd.class);
        job2.setNumReduceTasks(Integer.parseInt(args[1]));
        job2.setReducerClass(CityAVGStarsPerYear.class);
        job2.setOutputKeyClass(Text.class);
        job2.setOutputValueClass(Text.class);
        return (job2.waitForCompletion(true) ? 0 : 1);
    }


}
