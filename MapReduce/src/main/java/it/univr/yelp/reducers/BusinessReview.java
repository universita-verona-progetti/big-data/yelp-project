package it.univr.yelp.reducers;

import it.univr.yelp.TextPair;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class BusinessReview extends Reducer<TextPair, TextPair, Text, IntWritable> {

    /**
     * This reducer takes the output from UserBusinessReview as input and UserSumreview mappers
     * and it generates a new file for the next mapper in key-value format (Text-IntWritable).
     * The Text is the user_id
     * The IntWritable is the rating given by the user in his business review.
     * <p>
     * This reducer is used by queries 3 and 4.
     *
     * @param key     key of the received entities
     * @param values  list of values of the entities received
     * @param context allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */
    public void reduce(TextPair key, Iterable<TextPair> values, Reducer<TextPair, TextPair, Text, IntWritable>.Context context) throws IOException, InterruptedException {

        int rev = 0;
        for (TextPair tx : values) {
            if (rev == 0) {
                if (!tx.getFirst().toString().equals("")) {
                    break;
                }
                rev = Integer.parseInt(tx.getSecond().toString());
            } else {
                context.write(tx.getFirst(), new IntWritable(rev));
            }

        }

    }
}

