package it.univr.yelp.reducers;

import it.univr.yelp.TextPair;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class UserStarOcc extends Reducer<TextPair, TextPair, Text, Text> {

    /**
     * This reducer takes the output from BusinessStarBusiness and BusinessUserStarReview mappers as input
     * and it generates a new file for the next mapper in key-value format (Text-Text).
     * The first Text is the user_id
     * The second Text consists of 2 numbers separated by colon
     * The first one is the difference between the reviewed businesses rating and
     * the rating given by the user in that business review
     * The second one is the value 1 (one is used to count the number of reviews given by an user)
     * <p>
     * This reducer is used by queries 1 and 2.
     *
     * @param key     key of the received entities
     * @param values  list of values of the entities received
     * @param context allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    public void reduce(TextPair key, Iterable<TextPair> values, Context context) throws IOException, InterruptedException {
        float val = 0;
        for (TextPair tx : values) {
            if (val == 0) {
                if (!tx.getFirst().toString().equals("")) {
                    break;
                }
                val = Float.parseFloat(tx.getSecond().toString());
            } else {
                context.write(new Text(tx.getFirst().toString()), new Text(val - Float.parseFloat(tx.getSecond().toString()) + ";1"));
            }

        }


    }
}
