package it.univr.yelp.reducers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class UserNumreview extends Reducer<Text, IntWritable, Text, IntWritable> {

    /**
     * This reducer takes the output from UserOneReview and UserSumreview mappers as input
     * and it generates a new file for the next mapper in key-value format (Text-IntWritable).
     * The Text is the user_id
     * The IntWritable is the total number of review given by that user.
     * <p>
     * This reducer is used by queries 3 and 4.
     *
     * @param key     key of the received entities
     * @param values  list of values of the entities received
     * @param context allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    public void reduce(Text key, Iterable<IntWritable> values, Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {
        int somma = 0;
        for (IntWritable rec : values) {
            somma += Integer.parseInt(rec.toString());
        }
        context.write(key, new IntWritable(somma));
    }
}

