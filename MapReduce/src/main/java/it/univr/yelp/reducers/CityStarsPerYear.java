package it.univr.yelp.reducers;

import it.univr.yelp.TextPair;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class CityStarsPerYear extends Reducer<TextPair, TextPair, Text, Text> {

    /**
     * This reducer takes the output from BusinessCityBusiness and BusinessYearStarReview mappers as input
     * and it generates a new file for the next mapper in key-value format (Text-Text).
     * The first Text is the city name
     * The second text includes the review rating for a business in that city
     * and a number from 0 to 9 representing the year.
     * <p>
     * This reducer is used by query 6.
     *
     * @param key     key of the received entities
     * @param values  list of values of the entities received
     * @param context allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    public void reduce(TextPair key, Iterable<TextPair> values, Reducer<TextPair, TextPair, Text, Text>.Context context) throws IOException, InterruptedException {

        String city = "";

        for (TextPair tx : values) {
            if (city.equals("")) {
                if (!tx.getSecond().toString().equals("")) {
                    break;
                }
                city = tx.getFirst().toString();

            } else {
                context.write(new Text(city), new Text(tx.getSecond().toString() + ";" + tx.getFirst().toString()));
            }
        }
    }
}
