package it.univr.yelp.reducers;

import it.univr.yelp.TextPair;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class UserAVGStar extends Reducer<Text, TextPair, Text, FloatWritable> {

    /**
     * This reducer takes the output from UserStarOcc mapper as input and it generates the
     * result of the query.
     * The output is in key-value format (Text-FloatWritable).
     * The Text is the user_id
     * The FloatWritable is the average difference between the reviewed businesses rating
     * and the rating given by the user in a business review.
     * <p>
     * This reducer is used by queries 1 and 2.
     *
     * @param key     key of the received entities
     * @param values  list of values of the entities received
     * @param context allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    public void reduce(Text key, Iterable<TextPair> values, Reducer<Text, TextPair, Text, FloatWritable>.Context context) throws IOException, InterruptedException {

        int count = 0;
        float somma = (float) 0.0;


        for (TextPair txtValue : values) {
            count += Integer.parseInt(txtValue.getSecond().toString());
            somma += Float.parseFloat(txtValue.getFirst().toString());
        }
        context.write(key, new FloatWritable(somma / (float) count));

    }
}
