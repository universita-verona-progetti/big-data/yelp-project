package it.univr.yelp.reducers;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class BusinessAVGreview extends Reducer<Text, IntWritable, Text, FloatWritable> {

    /**
     * This reducer takes the output from BusinessSumrev mapper as input and it generates the
     * result of the query.
     * The output is in key-value format (Text-FloatWritable).
     * The Text is the business_id
     * The FloatWritable is the review rating average for that business.
     * <p>
     * This reducer is used by queries 3 and 4.
     *
     * @param key     key of the received entities
     * @param values  list of values of the entities received
     * @param context allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    public void reduce(Text key, Iterable<IntWritable> values, Reducer<Text, IntWritable, Text, FloatWritable>.Context context) throws IOException, InterruptedException {

        int count = 0;
        float somma = (float) 0.0;

        for (IntWritable rev : values) {
            count += 1;
            somma += Float.parseFloat(rev.toString());
        }
        context.write(key, new FloatWritable(somma / (float) count));

    }
}

