package it.univr.yelp.reducers;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class CityAVGStarsPerYear extends Reducer<Text, Text, Text, Text> {

    /**
     * This reducer takes the output from CityStarsPerYearOrd mapper as input and it generates the
     * result of the query.
     * The output is in key-value format (Text-Text).
     * The first Text is the city name
     * The second Text is the average review rating for the business of that city in the last 10 years,
     * grouped by year.
     * <p>
     * This reducer is used by query 6.
     *
     * @param key     key of the received entities
     * @param values  list of values of the entities received
     * @param context allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    public void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {

        int[] count = new int[10];
        float[] somma = new float[10];
        int i;
        float tmp;
        String[] parsertmp;
        StringBuilder result = new StringBuilder();


        for (Text txtValue : values) {
            parsertmp = txtValue.toString().split(";");
            i = Integer.parseInt(parsertmp[1]);
            tmp = Float.parseFloat(parsertmp[0]);
            somma[i] += tmp;
            if (tmp > 0.0)
                count[i]++;
        }

        for (i = 0; i < 10; i++) {
            if (count[i] != 0) {
                somma[i] /= count[i];
            }
            result.append(somma[i]);
            if (i != 9)
                result.append("\t");
        }

        context.write(key, new Text(result.toString()));
    }
}
