package it.univr.yelp.reducers;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class CityAVGstars extends Reducer<Text, FloatWritable, Text, FloatWritable> {

    /**
     * This reducer takes the output from CityStarsOrd mapper as input and it generates the
     * result of the query.
     * The output is in key-value format (Text-FloatWritable).
     * The Text is the city name
     * The FloatWritable is the average review rating for the business of that city.
     * <p>
     * This reducer is used by query 5.
     *
     * @param key     key of the received entities
     * @param values  list of values of the entities received
     * @param context allows the Mapper/Reducer to interact with the rest of the Hadoop system. It includes configuration data for the job as well as interfaces which allow it to emit output.
     * @throws IOException          Input/Output exception
     * @throws InterruptedException Interrupted exception
     */

    public void reduce(Text key, Iterable<FloatWritable> values, Reducer<Text, FloatWritable, Text, FloatWritable>.Context context) throws IOException, InterruptedException {

        int count = 0;
        float somma = (float) 0.0;


        for (FloatWritable txtValue : values) {
            count++;
            somma += Float.parseFloat(txtValue.toString());
        }
        context.write(key, new FloatWritable(somma / (float) count));

    }
}
