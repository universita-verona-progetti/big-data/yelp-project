package it.univr.yelp;

public class KeyPartitioner extends org.apache.hadoop.mapreduce.Partitioner<TextPair, TextPair> {

    /**
     * Uses TextPair methods to sort the keys and group them into the various partitions.
     *
     * @param key           the key to be partioned.
     * @param value         the entry value.
     * @param numPartitions the total number of partitions.
     * @return the number of the partition on which the key is to be placed
     */
    @Override
    public int getPartition(TextPair key, TextPair value, int numPartitions) {
        return (key.getFirst().hashCode() & Integer.MAX_VALUE) % numPartitions;
    }
}
