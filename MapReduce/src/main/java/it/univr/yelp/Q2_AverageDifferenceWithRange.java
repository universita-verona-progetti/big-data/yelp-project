package it.univr.yelp;

import it.univr.yelp.mappers.BusinessStarBusiness;
import it.univr.yelp.mappers.BusinessUserStarReview;
import it.univr.yelp.reducers.UserAVGStar;
import it.univr.yelp.reducers.UserStarOcc;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Answer to the {@link Q1_AverageDifference} with different time granularities, e.g., in the last month, year, two years.
 */

public class Q2_AverageDifferenceWithRange extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Q2_AverageDifferenceWithRange(), args);
        System.exit(res);

    }

    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        conf.set("start", args[2]);
        conf.set("end", args[3]);

        Job job1 = Job.getInstance(getConf(), "Q2_AverageDifferenceWithRange1");
        job1.setJarByClass(this.getClass());
        MultipleInputs.addInputPath(job1, new Path(args[0] + "/input/business.json"), TextInputFormat.class, BusinessStarBusiness.class);
        MultipleInputs.addInputPath(job1, new Path(args[0] + "/input/review.json"), TextInputFormat.class, BusinessUserStarReview.class);
        FileOutputFormat.setOutputPath(job1, new Path(args[0] + "/int1")); // eccezione se la directory esiste già, il job si ferma
        job1.setNumReduceTasks(Integer.parseInt(args[1]));   // setta il numero di reducer, forse è facoltativa, di default 1
        job1.setReducerClass(UserStarOcc.class);
        job1.setPartitionerClass(KeyPartitioner.class);
        job1.setMapOutputKeyClass(TextPair.class);
        job1.setMapOutputValueClass(TextPair.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(Text.class);
        job1.setGroupingComparatorClass(TextPair.FirstComparator.class);
        if (!job1.waitForCompletion(true))
            return 1;


        Job job2 = Job.getInstance(getConf(), "Q2_AverageDifferenceWithRange2");
        job2.setJarByClass(this.getClass());
        FileInputFormat.addInputPath(job2, new Path(args[0] + "/int1/part*"));
        FileOutputFormat.setOutputPath(job2, new Path(args[0] + "/output"));
        job2.setMapperClass(it.univr.yelp.mappers.UserStarOcc.class);
        job2.setNumReduceTasks(Integer.parseInt(args[1]));
        job2.setReducerClass(UserAVGStar.class);
        job2.setOutputKeyClass(Text.class);
        job2.setOutputValueClass(TextPair.class);
        return (job2.waitForCompletion(true) ? 0 : 1);

    }

}