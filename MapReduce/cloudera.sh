#!/usr/bin/env bash

classpath=$1
startdate=$2
enddate=$3
repositoryroot=$(git rev-parse --show-toplevel 2>/dev/null)
folder=$(echo "${classpath}" | cut -d'_' -f1)

# transfer jar file to cloudera
echo -e "Transferring jar file...\c"
docker cp --archive -L out/artifacts/yelp_jar/yelp.jar cloudera:/home/cloudera/yelp.jar
echo "OK"

# connect to cloudera and execute commands
echo -e "Cloudera execution started...\c"
docker exec -u cloudera cloudera bash -c " \
    hadoop fs -rm -r -f /user/cloudera/int*; \
    hadoop fs -rm -r -f /user/cloudera/output*; \
    hadoop jar /home/cloudera/yelp.jar it.univr.yelp.${classpath} /user/cloudera 1 ${startdate} ${enddate}; \
    hadoop fs -getmerge /user/cloudera/output/ /home/cloudera/output/map.txt; \
" >"${repositoryroot}"/Confronto_risultati/LOG/"${folder}"/cloudera_map_"$(date +'%s')".log 2>&1
echo "OK"

# get output
echo -e "Transferring output results...\c"
docker cp -a -L cloudera:/home/cloudera/output/map.txt "${repositoryroot}"/Confronto_risultati/Cloudera/"${folder}"/
echo "OK"

echo "DONE!"
exit 0
