#!/usr/bin/env bash

classpath=$1
startdate=$2
enddate=$3

repositoryroot=$(git rev-parse --show-toplevel 2>/dev/null)
folder=$(echo "${classpath}" | cut -d'_' -f1)
IFS=$'\r\n' GLOBIGNORE='*' command eval 'credentials=($(cat ${repositoryroot}/credentials))'
ipaddr=${credentials[0]}
user=${credentials[1]}
pass=${credentials[2]}

# send jar file to the cluster
echo -e "Transferring jar file...\c"
sshpass -p "${pass}" scp out/artifacts/yelp_jar/yelp.jar "${user}"@"${ipaddr}":/home/"${user}"/yelp/
echo "OK"

# connect to the cluster and execute commands
echo -e "Cluster execution started...\c"
sshpass -p "${pass}" ssh -o "StrictHostKeyChecking no" "${user}"@"${ipaddr}" " \
    cd yelp; \
    hadoop fs -rm -r -f /user/${user}/yelp/int*; \
    hadoop fs -rm -r -f /user/${user}/yelp/output*; \
    hadoop jar yelp.jar it.univr.yelp.${classpath} /user/${user}/yelp 1 ${startdate} ${enddate}; \
    hadoop fs -getmerge /user/${user}/yelp/output/ /home/${user}/yelp/output/map.txt; \
    cd output; \
    tar -czvf /home/${user}/yelp/output.tgz map.txt \
" >"${repositoryroot}"/Confronto_risultati/LOG/"${folder}"/cluster_map_"$(date +'%s')".log 2>&1
echo "OK"

# transfer output
echo -e "Transferring output results...\c"
sshpass -p "${pass}" scp "${user}"@"${ipaddr}":/home/"${user}"/yelp/output.tgz "${repositoryroot}"/Confronto_risultati/Cluster/"${folder}"
cd "${repositoryroot}"/Confronto_risultati/Cluster/"${folder}" || return
tar -xzvf output.tgz >/dev/null
rm output.tgz
echo "OK"

# delete output from cluster filesystem
echo -e "Delete output from cluster filesystem...\c"
sshpass -p "${pass}" ssh -o "StrictHostKeyChecking no" "${user}"@"${ipaddr}" "rm -rf /home/${user}/yelp/output*"
echo "OK"

echo "DONE!"
exit 0
