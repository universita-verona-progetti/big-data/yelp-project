\section{Introduzione} 
\label{intro}
    Il progetto realizzato consiste nell'analisi di un dataset pubblico, proveniente dalla piattaforma americana di recensioni Yelp \cite{yelp_dataset}, svolgendo determinate query.\\
    Lo scopo principale del progetto è quello di confrontare due tecniche di programmazione per l'analisi dei dati, sia per la complessità del codice scritto, sia per le prestazioni ottenute.\\
    Le tecniche che verranno utilizzare servono ad implementare applicazioni del tipo \textbf{Map Reduce}, rispettivamente in Java e PIG.
    
\subsection{Dataset}
    Il dataset è composto da diversi file in formato JSON ed è possibile trovare la documentazione sul sito ufficiale \cite{yelp_doc}.\\
    Per questa specifica analisi viene fornita una versione ridotta del dataset originale, composta da 3 files:
    \begin{itemize}
        \item \texttt{business.json}
        \item \texttt{review.json}
        \item \texttt{user.json}
    \end{itemize}
    \bigskip
    \noindent Il file \texttt{business.json} contiene una lista di business presenti sulla piattaforma, arricchita con varie informazioni su di esso. \\
    I parametri utilizzati nell'analisi sono:
    \begin{itemize}
        \item \texttt{business\_id} rappresenta il codice univoco per ogni business;
        \item \texttt{city} indica la città dove è situato il business;
        \item \texttt{stars} è la rappresentazione in stelle della valutazione media del business da 1 a 5.
    \end{itemize}
    
    \bigskip
    \noindent Il file \texttt{review.json} contiene una lista di recensioni scritte da utenti su specifici business. \\ I parametri utilizzati nell'analisi sono:
    \begin{itemize}
        \item \texttt{review\_id} rappresenta il codice univoco per ogni recensione;
        \item \texttt{user\_id} si riferisce all'identificativo dell'utente che ha scritto la recensione;
        \item \texttt{business\_id} è il codice univoco del business recensito;
        \item \texttt{stars} è la rappresentazione in stelle del voto, che va da 1 a 5, rilasciato dall'utente al business nella recensione;
        \item \texttt{date} indica la data e l'ora di pubblicazione della recensione.
    \end{itemize}
    \bigskip
    Il file \texttt{user.json} contiene una lista di utenti con i relativi dati, come il nome, la data d'iscrizione, la lista amici, un resoconto delle recensioni scritte. \\
    Dato che per l'analisi erano sufficienti le informazioni presenti in  \texttt{business.json} e  \texttt{review.json}, quest'ultimo file non è stato preso in considerazione.

\subsection{Definizione delle query}
    Le richieste da soddisfare sul dataset fornito sono le seguenti:
    \begin{enumerate}
        \item Per ogni utente che ha pubblicato una recensione, ritornare la differenza media tra il voto medio del business e il voto fornito dall'utente.\\
        \textbf{Esempio:} Se un utente ha recensito due business rispettivamente 3 e 3 stelle, e questi ultimi hanno come valutazione media rispettivamente 4.5 e 4.8 stelle.\\
        La differenza media si calcolerà in questo modo: ((4.5-3)+(4.8-3))/2.
        \item Rispondere alla domanda precedente scegliendo un qualsiasi arco temporale per le recensioni.
        \item Per ogni business ritornare il numero medio di recensioni fatte dagli utenti che hanno recensito quel business.\\
        \textbf{Esempio:} Se l'utente A e l'utente B hanno recensito il business C, l'utente A ha scritto 50 recensioni in totale e l'utente B ha scritto 30 recensioni in totale.\\
        Il numero medio delle recensioni fatte dagli utenti che hanno recensito C, si calcolerà in questo modo: (50+30)/2.
        \item Rispondere alla domanda precedente scegliendo un qualsiasi arco temporale per le recensioni.
        \item Per ogni città ritornare la valutazione media dei business in quella città scelto uno specifico arco temporale.\\
        \textbf{Esempio:} Se nell'ultimo anno nella città di San Francisco sono state scritte queste recensioni per i Business A, B, C:
        \begin{itemize}
            \item A, stars: 5
            \item C, stars: 2
            \item B, stars: 4
            \item A, stars: 4
        \end{itemize}
        \noindent Allora la valutazione media si calcolerà così: (5+2+4+4)/4.
        \item Rispondere alla domanda precedente elencando per ogni città la media delle recensioni dei business in quella città negli ultimi 10 anni.\\
        \textbf{Esempio:} Il calcolo si fa come nella domanda precedente con la differenza che il risultato sarà in questo formato:\textbf{\textit{ San Francisco 4.3; 2.8; 5; 4.1; 3.8; 4.3; 4.4; 3.4; 2.7; 4.9}}, dove i 10 numeri indicano la media annuale in ordine decrescente a partire dall'anno attuale.
    \end{enumerate}

\subsection{Struttura del framework Hadoop}
    Per elaborare grandi quantità di dati in modo distribuito in cluster di computer è consigliato l'utilizzo di framework specifici, come Hadoop. \\
    Si tratta di un framework composto principalmente da:
    \begin{itemize}
        \item \textbf{HDFS}: un file system distribuito, progettato appositamente per immagazzinare un’enorme quantità di dati, in modo da ottimizzare le operazioni di archiviazione e accesso a un ristretto numero di file di grandi dimensioni.
        \item \textbf{YARN}: gestisce la schedulazione dei lavori, allocando le risorse del cluster alle applicazioni in esecuzione, decidendo la priorità dei processi in caso di contesa per le risorse disponibili.\\ La tecnologia traccia e monitora l’avanzamento dell’elaborazione dei lavori.
        \item \textbf{Map Reduce}: rappresenta un paradigma di programmazione composto da tre fasi principali:
        \begin{enumerate}
            \item Map: in questa fase il mapper elabora i dati in input, sotto forma di file o directory, archiviati nell'HDFS. Il file in input viene passato riga per riga alla funzione di mapper che elabora i dati e ne crea diversi piccoli blocchi. 
            \item Shuffle: è la fase di trasferimento dei dati dai mapper ai reducer, durante la quale avviene anche l'ordinamento dei dati. Inoltre può iniziare prima del termine della fase di map, riducendo il tempo per il completamento delle attività.
            \item Reduce: in questa fase il reducer elabora i dati provenienti dal mapper e, una volta terminate le elaborazioni, produce un nuovo output che verrà archiviato nell'HDFS.
        \end{enumerate}
         L'unione di queste tre fasi rappresenta un \textbf{job}.
        \begin{figure}[ht!]
            \centering
            \includegraphics[width=0.80\textwidth]{img/mr1.png}
            \caption{Struttura di un job}
        \end{figure}
       
    \end{itemize}
    I vantaggi principali nell'utilizzo di Hadoop sono quelli di avere un'elevata affidabilità, in quanto anomalie e problemi vengono gestiti a livello applicativo, e una forte scalabilità, realizzabile semplicemente aggiungendo nodi al cluster.
    
    \newpage
    
    \subsection{Possibili implementazioni}
    \label{impl}
    Quando si analizzano grandi quantità di dati mediante l'utilizzo del paradigma \textbf{Map Reduce}, per prima cosa è necessario conoscere quali sono i vantaggi e gli svantaggi nell'utilizzare il linguaggio nativo di Hadoop, ovvero \textbf{Java}, oppure utilizzare \textbf{PIG}, una piattaforma ad alto livello che permette di scrivere in Pig Latin, un linguaggio di scripting ispirato ad SQL in grado di interrogare i dati mediante delle parole chiave.
    \bigskip
    
    \begin{table}[ht!]
        \begin{tabularx}{\linewidth}{>{\parskip1ex}X@{\kern4\tabcolsep}>{\parskip1ex}X}
            \toprule
            \hfil\bfseries PIG / PIG LATIN
            &
            \hfil\bfseries JAVA
            \\\cmidrule(r{3\tabcolsep}){1-1}\cmidrule(l{-\tabcolsep}){2-2}
            
            \textbf{Vantaggi:}\par
            + più facile da leggere e imparare;\par
            + richiede minor tempo di programmazione;\par%( e il 
            + lo scopo dell'analisi è chiaro e il contesto autoesplicativo;\par
            + mantenere il codice è più semplice;\par
            + non necessita di compilazione;\par
            + ottimizzazione automatica delle queries;\par
            + codice portabile.\par
            ~\par
            \textbf{Svantaggi:}\par
            - bug più difficili da scovare e complessi da risolvere;\par
            - l'ottimizzazione di queries complesse può non essere molto efficiente, creando molti job;\par
            - non è ideale per la manipolazione dei dati;\par
            - difficoltà nella gestione di media, serie di tempi, testo delimitato in maniera ambigua, dati di log e operazioni complesse su liste di oggetti non limitate, XML/JSON mal progettati e schemi molto flessibili.\par
            
            
            &
            
            
            \textbf{Vantaggi:}\par
            + maggior velocità di esecuzione dei job se implementati in maniera più efficiente rispetto all'ottimizzatore automatico di PIG;\par
            + ha più funzionalità;\par
            + possibilità di implementare query complesse con un numero inferiore di job.\par
            ~\par
            ~\par
            ~\par
            \textbf{Svantaggi:}\par
            - non tutti sono bravi a programmare in Java;\par
            - maggiore probabilità di riscontrare bug;\par
            - necessita di un numero maggiore di linee di codice;\par
            - maggior difficoltà nel mantenimento del codice;\par
            - i tempi di compilazione e distribuzione possono rallentare significativamente i miglioramenti incrementali.
            
           \\\bottomrule
        \end{tabularx}
        \caption{Vantaggi e svantaggi di PIG e Java}
    \end{table}
    
    \noindent Indipendentemente dalla tecnica di programmazione scelta per l'implementazione del codice, l'applicazione di tipo \textbf{Map Reduce} sarà un archivio JAR contenente del bytecode, il quale è il formato richiesto da Hadoop per l'esecuzione. 