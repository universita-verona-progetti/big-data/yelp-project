\section{Implementazione Map Reduce in Java}

L'implementazione del paradigma Map Reduce in Java si effettua mediante l'implementazione di tre classi principali: \textbf{Job Class}, \textbf{Map Class} e \textbf{Reduce Class}.\\
La \textbf{Job Class} (o anche \textbf{Driver Class}) è la classe più importante delle API fornite da Map Reduce e consente allo sviluppatore di configurare il job, eseguirlo, controllarne l'esecuzione e interrogarne lo stato.\\
Tra i metodi più rilevanti di questa classe troviamo:
\begin{itemize}
    \item \texttt{setJobName()}: imposta il nome;
    \item \texttt{setInputFormatClass()}: imposta il formato dei dati in input;
    \item \texttt{setOutputFormatClass()}: imposta il formato dei dati in output;
    \item \texttt{setMapperClass(Class)}: imposta il mapper;
    \item \texttt{setReducerClass(Class)}: imposta il reducer;
    \item \texttt{setPartitionerClass(Class)}: imposta il partitioner;
    \item \texttt{setOutputPath()}: imposta il percorso sul quale viene salvato l'output.
\end{itemize}

\noindent La \textbf{Map Class} associa le coppie chiave-valore di input a un set di coppie chiave-valore intermedie. 
Le mappe sono le singole attività che trasformano i 
record di input in record intermedi, i quali non devono 
necessariamente essere dello stesso tipo dei record di input.\\ 
Una data coppia di input può essere mappata su zero o su più coppie di output.\\
Il metodo principale, chiamato una volta per ogni coppia chiave-valore nella suddivisione dell'input, è il metodo:
\begin{lstlisting}
Map(KEYIN key,VALUEIN value,org.apache.hadoop.mapreduce.Mapper.Context context)
\end{lstlisting}
Nella \textbf{Reduce Class} si riduce un set di valori intermedi che condividono una chiave, con un set di valori più piccolo.\\Le implementazioni del reducer possono accedere alla configurazione per un job tramite il metodo \texttt{JobContext.getConfiguration()}.\\
Il metodo principale, chiamato una volta per ogni chiave nella raccolta di coppie chiave-valore, è il metodo:
\begin{lstlisting}
Reduce(KEYIN key, Iterable <VALUEIN> values, org.apache.hadoop.mapreduce.Reducer.Context context)
\end{lstlisting}  

\noindent Per sviluppare l'applicazione, oltre ad implementare le classi necessarie per il paradigma Map Reduce, è stato utilizzato un metodo della classe \textbf{MultipleInputs} all'interno della \textbf{Driver Class} per poter gestire più file in input, le classi \textbf{TextPair} e  \textbf{Custom Partitioner} per ordinare le chiavi in uscita dai mapper secondo le condizioni definite dall'utente e la tecnica \textbf{Reduce-side Join} per effettuare la join dei dati. 

\subsection{TextPair}
La scrittura di dati su HDFS richiede un utilizzo di tipi specifici tra i quali \texttt{Text} (stringhe), \texttt{IntWritable} (interi) e \texttt{FloatWritable} (float).\\
A causa della struttura dei file di input, per semplificare le operazioni di analisi dei dati, si è reso necessario l'utilizzo di un tipo di dato che non fa parte di quelli base di Hadoop, chiamato \texttt{TextPair}.\\
\texttt{TextPair} è composto da una coppia di \texttt{Text} e nel progetto è stato utilizzato soprattutto come chiave in uscita dai mapper, quando viene utilizzato il metodo della classe MultipleInputs.

\subsection{MultipleInputs}
Durante la fase di inizializzazione del job, normalmente viene assegnato un singolo file di input ad un singolo mapper. Essendo il dataset composto da file differenti, per struttura o contenuto, e avendo la necessità di lavorare su più file all'interno dello stesso job, si è deciso di utilizzare il metodo:
\begin{lstlisting}[language=xml]
addInputPath(JobConf conf,Path path,Class<? extends InputFormat> inputFormatClass), Class<? extends Mapper> mapperClass)
\end{lstlisting}
Questo metodo consente, nella fase di inizializzazione del job, di gestire più file in input ad esso, assegnandone uno per ogni mapper.
Al termine dell'esecuzione della fase di map, i singoli output di ogni mapper avranno la chiave strutturata nello stesso modo, in maniera da consentire prima l'ordinamento da parte del partitioner nella fase di shuffle e poi le successive elaborazioni dei dati da parte del reducer.

\subsection{Custom Partitioner}
Il Custom Partitioner è un processo che consente di indirizzare i risultati dei mapper in diversi reducer in base alle condizioni definite dall'utente e si attiva durante la fase di shuffle. \\
In questo caso specifico, impostando un Custom Partitioner su una chiave di tipo \texttt{TextPair}, è garantito che i record con la prima parte della chiave uguale andranno allo stesso reducer.\\
La semplificazione del processo di join e l'incremento dell'efficienza durante l'analisi dei dati con strutture differenti ha inciso sulla scelta di utilizzo di un Custom Partitioner.  \\
Per la sua implementazione è stato sufficiente fare l'override del metodo \texttt{getPartition()}, metodo di default della classe Partitioner di base, impostando sotto quali condizioni due chiavi devono essere mandate allo stesso reducer, come si può notare dal codice qui sotto.
\begin{figure}[ht!]
    \definecolor{mygreen}{rgb}{0,0.6,0}
    \definecolor{mygray}{rgb}{0.5,0.5,0.5}
    \definecolor{mymauve}{rgb}{0.58,0,0.82}
    
    \lstset{ %
      backgroundcolor=\color{white},   % choose the background color
      basicstyle=\footnotesize,        % size of fonts used for the code
      breaklines=true,                 % automatic line breaking only at whitespace
      captionpos=b,                    % sets the caption-position to bottom
      commentstyle=\color{mygreen},    % comment style
      escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
      keywordstyle=\color{blue},       % keyword style
      stringstyle=\color{mymauve},     % string literal style
    }
    
    
\begin{lstlisting}[frame=single, mathescape=true, language=java, caption={Codice custom partitioner}] 
public class KeyPartitioner extends org.apache.hadoop.mapreduce.Partitioner<TextPair, TextPair> {

    /**
     * Uses TextPair methods to sort the keys and group them into the various partitions.
     *
     * @param key           the key to be partioned.
     * @param value         the entry value.
     * @param numPartitions the total number of partitions.
     * @return the number of the partition on which the key is to be placed
     */
    @Override
    public int getPartition(TextPair key, TextPair value, int numPartitions) {
        return (key.getFirst().hashCode() & Integer.MAX_VALUE) % numPartitions;
    }
}
\end{lstlisting}  
\end{figure}

\newpage
\subsection{Join}
Esistono due tecniche per effettuare la join dei dati in Map Reduce: una durante la fase di map, chiamata \textbf{Map-side join}, e una durante la fase di reduce, chiamata \textbf{Reduce-side join}.

\subsubsection*{Map-side join}
    La Map-side join esegue la join prima che i dati vengano passati al mapper.\\ 
    La funzione Map richiede che i dati abbiano questi prerequisiti prima di poter effettuare la join:
    \begin{itemize}
        \item i dati devono essere partizionati e ordinati in un determinato modo;
        \item ogni dato di input deve essere diviso nello stesso numero di partizioni;
        \item devono essere ordinati con la stessa chiave;
        \item tutti i record per una specifica chiave devono risiedere nella stessa partizione.
    \end{itemize}
    

    \begin{figure}[ht!]
        \centering
        \includegraphics[scale=0.75]{img/Untitled Diagram.jpg}
        \caption{Schema Map-side Join}
    \end{figure}
    \noindent La Map-side join è più efficiente della Reduce-side join, ma richiede che i dati rispettino i prerequisiti sopra citati.

\subsubsection*{Reduce-side join}
In questo specifico caso è stato scelto di utilizzare la tecnica del Reduce-side join poiché non è stato possibile modificare la struttura dei dati prima di passarli come input, per far sì che rispettassero i prerequisiti per la Map-side join. Inoltre, si adatta alla tecnica del Custom Partitioner descritta prima.\\
Questa tecnica non è difficile da implementare in quanto i dati in uscita dal mapper vengono ordinati e raggruppati in liste in base alle chiavi dal partitioner.\\
Ogni reducer riceverà disgiuntamente le liste associate alle chiavi ed elaborerà i valori in essa contenuti, facendo così la join tra i dati.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.9\textwidth]{img/map_shuffle_reduce_2.jpg}
    \caption{Schema Reduce-side join}
\end{figure}

\newpage
\subsection{Pseudocodice query}
\label{pseudo}
\input{pseudojava/q1.tex}
\input{pseudojava/q2.tex}
\input{pseudojava/q3.tex}
\newpage
\input{pseudojava/q4.tex}
\input{pseudojava/q5.tex}
\input{pseudojava/q6.tex}
